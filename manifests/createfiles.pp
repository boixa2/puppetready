class puppetready::createfiles
{
  file { '/var/www/myproject/index.php':
    ensure  => 'present',
    source  => "puppet:///modules/puppetready/index_base.php",
    mode    => '0644',
  }

  file { '/var/www/myproject/info.php':
    ensure  => 'present',
    source => "puppet:///modules/puppetready/index.php",
    mode    => '0644',
  }
}
